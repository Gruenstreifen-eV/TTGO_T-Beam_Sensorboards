EESchema Schematic File Version 4
LIBS:Sensor_v1_4ADC-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ADC Board"
Date "2019-09-09"
Rev "1"
Comp "Grünstreifen eV"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 7250 2650
NoConn ~ 7250 3150
NoConn ~ 7250 2750
NoConn ~ 7250 2850
NoConn ~ 7250 3250
NoConn ~ 7250 3350
NoConn ~ 7250 3650
NoConn ~ 7250 3750
NoConn ~ 7250 3850
NoConn ~ 7250 4150
NoConn ~ 7250 4250
NoConn ~ 7250 4350
$Comp
L Connector:Conn_01x10_Female J1
U 1 1 5D6E034A
P 6050 3150
F 0 "J1" V 6215 3080 50  0000 C CNN
F 1 "Conn_01x10_ADS1115" V 6124 3080 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x10_P2.54mm_Vertical" H 6050 3150 50  0001 C CNN
F 3 "~" H 6050 3150 50  0001 C CNN
	1    6050 3150
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J7
U 1 1 5D6F20CF
P 7250 4250
F 0 "J7" V 7312 4394 50  0000 L CNN
F 1 "ADC3" V 7150 4150 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 7250 4250 50  0001 C CNN
F 3 "~" H 7250 4250 50  0001 C CNN
	1    7250 4250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J6
U 1 1 5D6F16BB
P 7250 3750
F 0 "J6" V 7312 3894 50  0000 L CNN
F 1 "ADC2" V 7150 3650 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 7250 3750 50  0001 C CNN
F 3 "~" H 7250 3750 50  0001 C CNN
	1    7250 3750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J5
U 1 1 5D6F1021
P 7250 3250
F 0 "J5" V 7312 3394 50  0000 L CNN
F 1 "ADC1" V 7150 3150 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 7250 3250 50  0001 C CNN
F 3 "~" H 7250 3250 50  0001 C CNN
	1    7250 3250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 5D6EEBEE
P 7250 2750
F 0 "J4" V 7312 2894 50  0000 L CNN
F 1 "ADC0" V 7150 2650 50  0000 L CNN
F 2 "Connector_JST:JST_XH_B3B-XH-A_1x03_P2.50mm_Vertical" H 7250 2750 50  0001 C CNN
F 3 "~" H 7250 2750 50  0001 C CNN
	1    7250 2750
	-1   0    0    1   
$EndComp
NoConn ~ 6250 3050
$Comp
L Connector:Conn_01x13_Female J3
U 1 1 5D742435
P 5150 3450
F 0 "J3" H 4800 4050 50  0000 L CNN
F 1 "Conn_01x13_Female" H 4700 4200 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 5150 3450 50  0001 C CNN
F 3 "~" H 5150 3450 50  0001 C CNN
	1    5150 3450
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x13_Female J2
U 1 1 5D7438DB
P 4500 3450
F 0 "J2" H 4150 4050 50  0000 C CNN
F 1 "Conn_01x13_Female" H 4300 4200 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 4500 3450 50  0001 C CNN
F 3 "~" H 4500 3450 50  0001 C CNN
	1    4500 3450
	1    0    0    -1  
$EndComp
NoConn ~ 5350 2850
NoConn ~ 5350 2950
NoConn ~ 5350 3050
NoConn ~ 5350 3150
NoConn ~ 5350 3250
NoConn ~ 5350 3850
NoConn ~ 5350 3950
NoConn ~ 4300 3850
NoConn ~ 4300 3750
NoConn ~ 4300 3650
NoConn ~ 4300 3550
NoConn ~ 4300 3350
NoConn ~ 4300 3250
NoConn ~ 4300 3150
NoConn ~ 4300 3050
NoConn ~ 4300 2950
NoConn ~ 4300 2850
NoConn ~ 5350 3450
Text GLabel 6250 3550 2    50   Input ~ 0
VCC
Text GLabel 4300 4050 0    50   Input ~ 0
VCC
Text GLabel 7050 2650 0    50   Input ~ 0
GND
Text GLabel 4300 3950 0    50   Input ~ 0
GND
Text GLabel 5350 3350 2    50   Input ~ 0
GND
Text GLabel 5350 3550 2    50   Input ~ 0
GND
Text GLabel 5350 3750 2    50   Input ~ 0
SDA
Text GLabel 5350 3650 2    50   Input ~ 0
SCL
Text GLabel 7050 4350 0    50   Input ~ 0
VCC
Text GLabel 7050 3850 0    50   Input ~ 0
VCC
Text GLabel 7050 3350 0    50   Input ~ 0
VCC
Text GLabel 7050 2850 0    50   Input ~ 0
VCC
Text GLabel 7050 3650 0    50   Input ~ 0
GND
Text GLabel 7050 4150 0    50   Input ~ 0
GND
Text GLabel 7050 3150 0    50   Input ~ 0
GND
Text GLabel 6250 3150 2    50   Input ~ 0
GND
Text GLabel 6250 3450 2    50   Input ~ 0
GND
Text GLabel 7050 4250 0    50   Input ~ 0
ADC3
Text GLabel 6250 2650 2    50   Input ~ 0
ADC3
Text GLabel 7050 3750 0    50   Input ~ 0
ADC2
Text GLabel 6250 2750 2    50   Input ~ 0
ADC2
Text GLabel 7050 3250 0    50   Input ~ 0
ADC1
Text GLabel 6250 2850 2    50   Input ~ 0
ADC1
Text GLabel 6250 2950 2    50   Input ~ 0
ADC0
Text GLabel 7050 2750 0    50   Input ~ 0
ADC0
Text GLabel 6250 3350 2    50   Input ~ 0
SCL
Text GLabel 6250 3250 2    50   Input ~ 0
SDA
Wire Notes Line
	4400 2800 5250 2800
Wire Notes Line
	5250 2800 5250 4200
Wire Notes Line
	5250 4200 4400 4200
Wire Notes Line
	4400 4200 4400 2800
Text Notes 4550 4300 0    50   ~ 0
TTGO-TBeam
Wire Wire Line
	5350 4050 5400 4050
Wire Wire Line
	5400 4050 5400 4400
Wire Wire Line
	5400 4400 3900 4400
Wire Wire Line
	3900 4400 3900 3450
Wire Wire Line
	3900 3450 4300 3450
$EndSCHEMATC
