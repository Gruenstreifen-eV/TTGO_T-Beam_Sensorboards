EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "TTGO-TBeam_Sensor_SDS_BME"
Date "2019-09-09"
Rev "1"
Comp "Grünstreifen eV"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
NoConn ~ 3900 3700
Wire Wire Line
	5250 4400 5250 4350
NoConn ~ 5950 3150
NoConn ~ 5950 3250
NoConn ~ 5950 3350
NoConn ~ 5950 3450
NoConn ~ 5950 3550
NoConn ~ 5950 3750
NoConn ~ 5950 4150
NoConn ~ 5950 4250
NoConn ~ 4950 4150
NoConn ~ 4950 3850
NoConn ~ 4950 3650
NoConn ~ 4950 3550
NoConn ~ 4950 3450
NoConn ~ 4950 3350
NoConn ~ 4950 3250
NoConn ~ 4950 3150
NoConn ~ 4950 4250
NoConn ~ 5950 3650
Text GLabel 4950 4350 0    50   Input ~ 0
VCC
Text GLabel 3900 3600 2    50   Input ~ 0
VCC
Text GLabel 5950 3850 2    50   Input ~ 0
GND
Text GLabel 7050 3450 0    50   Input ~ 0
GND
Text GLabel 7050 3350 0    50   Input ~ 0
VCC
Text GLabel 7050 3550 0    50   Input ~ 0
SCL
Text GLabel 5950 3950 2    50   Input ~ 0
SCL
Text GLabel 7050 3650 0    50   Input ~ 0
SDA
Text GLabel 5950 4050 2    50   Input ~ 0
SDA
$Comp
L Connector:Conn_01x13_Female J3
U 1 1 5D7892FD
P 5750 3750
F 0 "J3" H 5400 4400 50  0000 C CNN
F 1 "Conn_01x13_Female" H 5550 4550 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 5750 3750 50  0001 C CNN
F 3 "~" H 5750 3750 50  0001 C CNN
	1    5750 3750
	-1   0    0    -1  
$EndComp
Text GLabel 3900 3500 2    50   Input ~ 0
GND
$Comp
L Connector:Conn_01x13_Female J2
U 1 1 5D78D7ED
P 5150 3750
F 0 "J2" H 4750 4400 50  0000 L CNN
F 1 "Conn_01x13_Female" H 4800 4550 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x13_P2.54mm_Vertical" H 5150 3750 50  0001 C CNN
F 3 "~" H 5150 3750 50  0001 C CNN
	1    5150 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J4
U 1 1 5D79051D
P 7250 3450
F 0 "J4" H 7000 3800 50  0000 L CNN
F 1 "Conn_01x04_BME" H 6800 3700 50  0000 L CNN
F 2 "Connector_JST:JST_XH_S4B-XH-A-1_1x04_P2.50mm_Horizontal" H 7250 3450 50  0001 C CNN
F 3 "~" H 7250 3450 50  0001 C CNN
	1    7250 3450
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x05_Female J1
U 1 1 5D793739
P 3700 3500
F 0 "J1" H 3592 3885 50  0000 C CNN
F 1 "Conn_01x05_SDS011" H 3592 3794 50  0000 C CNN
F 2 "Connector_JST:JST_XH_S5B-XH-A-1_1x05_P2.50mm_Horizontal" H 3700 3500 50  0001 C CNN
F 3 "~" H 3700 3500 50  0001 C CNN
	1    3700 3500
	-1   0    0    -1  
$EndComp
Text Notes 5200 4400 0    63   ~ 0
36\n39\nRST\n34\n35\n32\n33\n25\n14\n13\n02\nGND\n5V
Text Notes 5500 4400 0    63   ~ 0
TXD\nRXD\n23\n4\n0\nGND\n3.3V\nGND\n22\n21\n3.3\nLora2\nLora1
Text GLabel 4950 3950 0    50   Input ~ 0
SDS_RX
Text GLabel 3900 3400 2    50   Input ~ 0
SDS_RX
Text GLabel 4950 4050 0    50   Input ~ 0
SDS_TX
Text GLabel 3900 3300 2    50   Input ~ 0
SDS_TX
Text Notes 5250 4600 0    50   ~ 0
TTGO-TBeam
Wire Notes Line
	5850 3050 5850 4450
Wire Notes Line
	5850 4450 5050 4450
Wire Notes Line
	5050 4450 5050 3050
Wire Notes Line
	5050 3050 5850 3050
Wire Wire Line
	5950 4350 6100 4350
Wire Wire Line
	6100 4350 6100 4750
Wire Wire Line
	6100 4750 4450 4750
Wire Wire Line
	4450 4750 4450 3750
Wire Wire Line
	4450 3750 4950 3750
$EndSCHEMATC
